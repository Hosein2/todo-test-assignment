import { TodoForm } from "./TodoForm"
import { TodoListSummary } from "./TodoListSummary"

export const TodoOverview = () => {
    return (
        <>
            <TodoForm />
            <TodoListSummary />
        </>
    )
}
