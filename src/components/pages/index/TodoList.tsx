import { RootState } from "@/app/store"
import { Todo, moveCard } from "@/app/todoSlice"
import { ListItem } from "@mui/material"
import { useDispatch, useSelector } from "react-redux"
import DoneIcon from '@mui/icons-material/Done';
import { FilterTodo } from "./FilterTodo";
import QuestionMarkOutlinedIcon from '@mui/icons-material/QuestionMarkOutlined';
import { useCallback, useMemo } from "react";
import { EmptyTodo } from "@/components/shared/EmptyTodo";
import { useDragAndDrop } from "@/hook";

export const TodoList = () => {
  const dispatch = useDispatch()

  const moveCardHandler = useCallback((dragIndex: number, hoverIndex: number) => {
    dispatch(moveCard({ dragIndex, hoverIndex }))
  }, [])

  const RenderTodo = useCallback(
    (todo: Todo, index: number) => {
      return (
        <TodoItem key={todo.id} text={todo.text} status={todo.status} index={index} moveCard={moveCardHandler} id={todo.id} />
      )
    },
    [],
  )

  const state = useSelector((state: RootState) => state.todo)
  console.log(state.todoList)

  const todos = useMemo(() => {
    return state.todoList.filter(todo => todo.status === state.filter)
  }, [state.todoList, state.filter])

  if (state.todoList.length === 0) return <EmptyTodo>
    There is no any todo in your list
  </EmptyTodo>

  return (
    <>
      <FilterTodo />
      {todos.length !== 0 ? todos.map((todo, index) => RenderTodo(todo, index)) : <EmptyTodo>
        There is no any todo in {state.filter ?? "not completed"}
      </EmptyTodo>}
    </>
  )
}

interface TodoItemProp {
  text: string;
  status: "completed" | null;
  id: number;
  moveCard: (dragIndex: number, hoverIndex: number) => void
  index: number;
}

const TodoItem = ({ text, status, moveCard, index, id }: TodoItemProp) => {
  const { handlerId, opacity, ref } = useDragAndDrop({ id, index, moveCard })

  return (
    <ListItem ref={ref as any} sx={{ display: 'flex', alignItems: "center", justifyContent: "space-between", opacity, padding: 4, cursor: "pointer" }} data-handler-id={handlerId}>
      {text}

      {status === "completed" ? <DoneIcon sx={{ color: 'green' }} /> : <QuestionMarkOutlinedIcon sx={{ color: 'red' }} />}

    </ListItem>
  )
}