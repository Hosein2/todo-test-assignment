import { createTodo } from "@/app/todoSlice"
import { Box, Button, TextField } from "@mui/material"
import { useState } from "react"
import { useDispatch } from "react-redux"
import AddIcon from '@mui/icons-material/Add';
export const TodoForm = () => {
    const dispatch = useDispatch()
    const [todo, setTodo] = useState<string>("")
    const [showForm, setShowForm] = useState(false)

    const submitForm = (e: React.SyntheticEvent) => {
        e.preventDefault()

        if (!todo) return;

        dispatch(createTodo(todo))
        setTodo("")
    }

    return (
        <>
            <Button onClick={() => {
                setShowForm((prev) => !prev)
            }}
                variant="contained" sx={{ position: 'absolute', bottom: '-15px', left: '50%', transform: 'translate(-50%)', zIndex: 2 }}>
                {
                    showForm ? "- Hide Form" : "+ New Task"
                }
            </Button>

            {
                showForm ?
                    <form onSubmit={submitForm}>
                        <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between', padding: 4, position: 'relative' }}>
                            <TextField value={todo} placeholder="type your todo..." variant="standard" label="todo" onChange={(e) => {
                                setTodo(e.target.value)
                            }} sx={{ width: '100%' }} />

                            <Button variant="text" type="submit" sx={{ position: 'absolute', right: 0, color: '#000' }}>
                                <AddIcon />
                            </Button>
                        </Box>
                    </form> : null
            }
        </>
    )
}