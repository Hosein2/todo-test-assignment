import { TabContent } from "@/components/shared";
import { Box, Tab as MuiTab, Tabs } from "@mui/material";
import { useState } from "react";
import { TodoOverview } from "./TodoOverview";
import { TodoList } from "./TodoList";
import FormatListBulletedIcon from '@mui/icons-material/FormatListBulleted';
import ChecklistIcon from '@mui/icons-material/Checklist';

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

export default function TodoTab() {
  const [value, setValue] = useState(0);

  const handleChange = (_: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  return (
    <Box sx={{ minWidth: '23rem', background: "#fff", borderRadius: 2, boxShadow: '0', paddingX: 4, paddingTop: 4, paddingBottom: 10, position: 'relative' }}>
      <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
        <Tabs value={value} onChange={handleChange} aria-label="todo tabs" sx={{
          ".MuiTabs-flexContainer": {
            display: 'flex', alignItems: 'center', justifyContent: "space-between"
          }
        }}>
          <MuiTab label={<FormatListBulletedIcon sx={{ color: "black" }} />} {...a11yProps(0)} />
          <MuiTab label={<ChecklistIcon sx={{ color: "black" }} />} {...a11yProps(1)} />
        </Tabs>
      </Box>
      <Box sx={{ mt: 4 }}>
        <TabContent value={value} index={0}>
          <TodoOverview />
        </TabContent>
        <TabContent value={value} index={1}>
          <TodoList />
        </TabContent>
      </Box>
    </Box>
  );
}