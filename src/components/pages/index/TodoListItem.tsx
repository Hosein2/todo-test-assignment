import { Todo, removeTodo, updateTodo } from '@/app/todoSlice'
import DeleteIcon from '@mui/icons-material/Delete';
import { ListItem, Typography } from '@mui/material';
import { useDispatch } from 'react-redux';
interface Props {
  todo: Todo
}

export const TodoListItem = ({ todo }: Props) => {
  const { text, id, status } = todo
  const dispatch = useDispatch()

  const deleteTodo = () => {
    dispatch(removeTodo(id))
  }

  const updateTodoHandler = () => {
    dispatch(updateTodo(id))
  }

  return (
    <ListItem sx={{ display: 'flex', alignItems: 'center', justifyContent: "space-between", cursor: 'pointer' }} onClick={updateTodoHandler}>
      <Typography component="span" sx={{ textDecoration: status === "completed" ? "line-through" : "none", }}>
        {text}
      </Typography>

      <button onClick={deleteTodo}>
        <DeleteIcon sx={{ color: 'red' }} />
      </button>
    </ListItem>
  )
}
