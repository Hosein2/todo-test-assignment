import { Box } from '@mui/material'
import TodoTab from './TodoTabs'

export const Container = () => {
    return (
        <Box component="div" sx={{ padding: 16, display: "flex", alignItems: "center", justifyContent: 'center', flexDirection: "column" }}>
            <TodoTab />
        </Box>
    )
}
