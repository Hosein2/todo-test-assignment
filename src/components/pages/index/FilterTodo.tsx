import { RootState } from "@/app/store"
import { Status, filterTodos } from "@/app/todoSlice"
import { Box, Button } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'

export const FilterTodo = () => {
    const filterButtons = [{ text: "Completed", value: 'completed', id: 1 }, { text: "Not Completed", value: null, id: 2 }]
    const dispatch = useDispatch()
    const filter = useSelector((state: RootState) => state.todo.filter)
    return (
        <Box sx={{ mt: 4, mb: 8 }}>
            {filterButtons.map(({ text, value, id }) =>

                <Button key={id} onClick={() => {
                    dispatch(filterTodos(value as Status))
                }}
                    variant={filter === value ? "contained" : "outlined"}
                    sx={{ mx: 2 }}>
                    {text}
                </Button>
            )}
        </Box>
    )
}
