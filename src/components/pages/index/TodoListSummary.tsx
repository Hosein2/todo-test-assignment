import { RootState } from '@/app/store'
import { useSelector } from 'react-redux'
import { TodoListItem } from '..'
import { EmptyTodo } from '@/components/shared/EmptyTodo'

export const TodoListSummary = () => {
  const todoList = useSelector((state: RootState) => state.todo.todoList)

  if (todoList.length === 0) return (<EmptyTodo>Todo list is empty</EmptyTodo>)

  return (
    <>{todoList.map((todo) => <TodoListItem key={todo.id} todo={todo} />)}</>
  )
}
