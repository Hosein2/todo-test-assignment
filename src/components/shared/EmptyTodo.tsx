import { Typography } from '@mui/material'
import { PropsWithChildren } from 'react'

export const EmptyTodo = ({ children }: PropsWithChildren) => {
    return <Typography component="h3" sx={{ textAlign: 'center', fontSize: '1rem' }}>{children}</Typography>
}
