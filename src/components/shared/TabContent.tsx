interface Props {
    children?: React.ReactNode;
    index: number;
    value: number;
}

export const TabContent = ({ children, index, value, ...props }: Props) => {
    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`tab-${index}`}
            aria-labelledby={`tab-${index}`}
            {...props}
        >
            {value === index && (
                <>{children}</>
            )}
        </div>
    );
}