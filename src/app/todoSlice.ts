import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import update from 'immutability-helper';

export interface Todo {
  text: string;
  id: number;
  status: Status;
}

export interface TodoState {
  todoList: Todo[] | [];
  filter: Status;
}

const initialState: TodoState = {
  todoList: [],
  filter: null,
};

export interface Item {
  id: number;
  text: string;
}

export type Status = 'completed' | null;

export const todoSlice = createSlice({
  name: 'counter',
  initialState,
  reducers: {
    createTodo: (state, action: PayloadAction<string>) => {
      const newTodo = {
        text: action.payload,
        id: Math.floor(Math.random() * 1000),
        status: null,
      };

      state.todoList = [...state.todoList, newTodo];
    },
    removeTodo: (state, action: PayloadAction<number>) => {
      state.todoList = state.todoList.filter(
        (todo) => todo.id !== action.payload,
      );
    },
    updateTodo: (state, action: PayloadAction<number>) => {
      state.todoList = state.todoList.map((todo) =>
        todo.id === action.payload
          ? {
              ...todo,
              status: todo.status === 'completed' ? null : 'completed',
            }
          : todo,
      );
    },
    filterTodos(state, action: PayloadAction<Status>) {
      state.filter = action.payload;
    },
    moveCard(
      state,
      action: PayloadAction<{
        dragIndex: number;
        hoverIndex: number;
      }>,
    ) {
      state.todoList = update(state.todoList, {
        $splice: [
          [action.payload.dragIndex, 1],
          [
            action.payload.hoverIndex,
            0,
            state.todoList[action.payload.dragIndex] as Todo,
          ],
        ],
      });
    },
  },
});

export const { createTodo, removeTodo, updateTodo, filterTodos, moveCard } =
  todoSlice.actions;

export default todoSlice.reducer;
