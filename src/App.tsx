import { Routes, Route } from "react-router-dom"
import { Home } from './pages'
import { Box } from "@mui/material"

export const App = () => {
  return (
    <Box className="App" sx={{ background: "linear-gradient(to right,#A770EF, #CF8BF3, #FDB99B)", minHeight: '100vh', minWidth: "100vw" }}>
      <Routes>
        <Route path="/" element={<Home />} />
      </Routes>
    </Box>
  )
}
